#!/bin/sh

set -e

work_dir="$(mktemp -d)"
systemctl start apache2

if ! systemctl is-active -q apache2; then
    echo "Error: apache2.service failed to start"
    exit 1
fi

cd ${work_dir}

humble -u http://127.0.1.1 -o pdf
if [ ! -e ${work_dir}/humble_http_127.0.1.1*.pdf ]; then
    echo "The report in pdf does not exist"
    exit 1
fi

humble -u http://127.0.1.1 -o txt
if [ ! -e ${work_dir}/humble_http_127.0.1.1*.txt ]; then
    echo "The report in txt does not exist"
    exit 1
fi

humble -u http://127.0.1.1 -o html
if [ ! -e ${work_dir}/humble_http_127.0.1.1*.html ]; then
    echo "The report in html does not exist"
    exit 1
fi

systemctl stop apache2
rm -rf $work_dir
